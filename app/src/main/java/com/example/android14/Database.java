package com.example.android14;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Handles the database
 *
 * @author Jason Cheng
 */
public class Database {
    public static final String Dir = "/data/data/com.example.android14/albums.dat";

    /*
    Reads an object from memory
     */
    //User
    public static ArrayList<Album> read() throws IOException, ClassNotFoundException {
        try {
            //Apparently this creates albums.dat automatically further testing required to corroborate
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(Dir));
            ArrayList<Album> list = (ArrayList<Album>) ois.readObject();
            ois.close();
            return list;
        } catch (Exception e) {
            return new ArrayList<Album>();
        }
    }

    /*
    Writes an object into memory
     */
    //Admin & User
    public static void write (ArrayList<Album> obj) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream (new FileOutputStream(Dir));
        oos.writeObject(obj);//I should write the whole array back
        oos.close();
    }
}

