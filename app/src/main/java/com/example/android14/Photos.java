package com.example.android14;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

/**
 * List of Photos Class
 * Allows users to move, delete, import, open photos and play albums
 *
 * @author Ashleigh Chung, Jason Cheng
 */
public class Photos extends AppCompatActivity implements ConfirmFragment.ConfirmFragmentListener{

    GridView gridView;
    GridViewAdapter adapter;
    public static final String ALBUM_INDEX = "albumIndex";
    public static ArrayList<Photo> listOfPhotos;
    public static int curr_index = -1;
    Button movePhoto, openPhoto, deletePhoto, addPhoto, slideshow;

    Photo photo;
    public static final int REQUEST_OPEN_RESULT_CODE = 0;
    public static final int MOVE_PHOTO_CODE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photos_grid);

        //get the fields
        listOfPhotos = getIntent().getParcelableArrayListExtra("list");

        gridView = (GridView)findViewById(R.id.listOfPhotos);
        gridView.setDrawSelectorOnTop(true);
        adapter = new GridViewAdapter(listOfPhotos,this);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener((p, V, pos, id) -> curr_index = pos);

        //Set up buttons
        movePhoto = (Button) findViewById(R.id.movePhoto);
        openPhoto = (Button) findViewById(R.id.openPhoto);
        deletePhoto = (Button) findViewById(R.id.deletePhoto);
        addPhoto = (Button) findViewById(R.id.addPhoto);
        slideshow = (Button) findViewById(R.id.slideshow);

        movePhoto.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){ movePhoto(curr_index);}
        });
        openPhoto.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){ openPhoto(curr_index);}
        });
        deletePhoto.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){ deletePhoto(curr_index);}
        });
        addPhoto.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(stockCheck()) {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.setType("image/*");
                    startActivityForResult(intent, REQUEST_OPEN_RESULT_CODE);
                }
            }
        });
        slideshow.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){ openSlideshow();}
        });

    }

    //For playing an album
    public void openSlideshow(){
        if(listOfPhotos.size() != 0){
            Intent intent = new Intent(this, Slideshow.class);
            intent.putParcelableArrayListExtra("list", listOfPhotos);
            startActivity(intent);
        }else{
            Toast.makeText(this, "Add Pictures First", Toast.LENGTH_SHORT).show();
        }
    }

    //For moving a photo
    public void movePhoto(int pos){
        try {
            Album curr_Album = Albums.albums.get(Albums.curr_index);
            if(curr_Album.getAlbumName().equals("stock") && curr_Album.getListOfPhotos().size() == 5){
                Toast.makeText(this, "Cannot have less than 5 pictures in Stock", Toast.LENGTH_SHORT).show();
                return;
            }
            photo = listOfPhotos.get(pos);
            Bundle bundle = new Bundle();
            Intent intent = new Intent(this, MovePhoto.class);

            bundle.putInt(MovePhoto.ALBUM_INDEX, pos);
            intent.putExtras(bundle);

            startActivityForResult(intent, MOVE_PHOTO_CODE);
        }catch (Exception e){
            Toast.makeText(this, "Select a Photo First", Toast.LENGTH_SHORT).show();
        }
    }

    //For opening a photo
    public void openPhoto(int pos){
        try {
            Photo photo = listOfPhotos.get(pos);

            Intent intent = new Intent(this, PhotoDisplay.class);
            intent.putExtra("singlePhoto", (Parcelable) photo);
            startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(this, "Select a Photo First", Toast.LENGTH_SHORT).show();
        }
    }

    //For deleting a photo
    public void deletePhoto(int pos){
        if(curr_index != -1){
            Album curr_Album = Albums.albums.get(Albums.curr_index);
            if(curr_Album.getAlbumName().equals("stock") && curr_Album.getListOfPhotos().size() == 5){
                Toast.makeText(this, "Cannot have less than 5 pictures in Stock", Toast.LENGTH_SHORT).show();
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString(ConfirmFragment.MESSAGE_KEY,
                    "Are you sure that you want to delete this photo?");
            DialogFragment newFragment = new ConfirmFragment();
            newFragment.setArguments(bundle);
            newFragment.show(getSupportFragmentManager(), "badfields");
            return;// does not quit activity, just returns from method
        }else{
            Toast.makeText(this, "Select a Photo First", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onYesClicked(){
        listOfPhotos.remove(curr_index);
        adapter = new GridViewAdapter(listOfPhotos,this);
        gridView.setAdapter(adapter);
        curr_index = -1;
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            Album curr_Album = Albums.albums.get(Albums.curr_index);
            curr_Album.listOfPhotos = listOfPhotos;
            Database.write((ArrayList<Album>) Albums.albums);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        super.onActivityResult(requestCode, resultCode, intent);
        if(requestCode == REQUEST_OPEN_RESULT_CODE && resultCode == RESULT_OK){
            Uri uri = null;
            if(intent != null){
                uri = intent.getData();
                String photoPath = uri.getPath();
                String path = uri.getPath();
                String fileName = path.substring(path.lastIndexOf('/') + 1);


                Photo newPhoto = new Photo(photoPath, fileName, uri.toString());
                listOfPhotos.add(newPhoto);

                gridView.setAdapter(new GridViewAdapter(listOfPhotos,this));
            }
        }else if(requestCode == MOVE_PHOTO_CODE && resultCode == RESULT_OK){
            Bundle bundle = intent.getExtras();
            if(bundle == null){
                return;
            }
            String name = bundle.getString(MovePhoto.ALBUM_NAME);

            Album curr_Album = Albums.albums.get(Albums.curr_index);
            if(name.equals("stock") && Albums.albums.get(0).getListOfPhotos().size() == 10){
                Toast.makeText(this, "Cannot have more than 10 pictures in Stock", Toast.LENGTH_SHORT).show();
                return;
            }
            int index = bundle.getInt(MovePhoto.ALBUM_INDEX);
            listOfPhotos.remove(index);
            found:
            {
                for(int i = 0; i < Albums.albums.size(); i++){
                    if(Albums.albums.get(i).getAlbumName().equals(name)){
                        Albums.albums.get(i).getListOfPhotos().add(photo);
                        break found;
                    }
                }
                Album new_Album = new Album(name);
                Albums.albums.add(new_Album);
                new_Album.getListOfPhotos().add(photo);
            }
            gridView.setAdapter(new GridViewAdapter(listOfPhotos,this));
        }
        curr_index = -1;
    }

    //Stock album Constraint
    public boolean stockCheck(){
        Album curr_Album = Albums.albums.get(Albums.curr_index);
        if(curr_Album.getAlbumName().equals("stock") && curr_Album.getListOfPhotos().size() == 10){
            Toast.makeText(this, "Cannot have more than 10 pictures in Stock", Toast.LENGTH_SHORT).show();
            return false;
        }else{
            return true;
        }
    }
}


