package com.example.android14;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Search Class
 * Allows users to search for photos across all albums based on their tags
 *
 * @author Ashleigh Chung
 */
public class Search extends AppCompatActivity {

    private ArrayList<Album> listOfAlbums;
    private ArrayList<Photo> searchedPhotos;

    EditText personSearch, locationSearch;
    RadioGroup searchOrAnd;
    RadioButton orAnd;
    Button searchButton;
    GridView searchedPhotosGrid;
    GridViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);

        //get the list of all albums
        listOfAlbums = getIntent().getParcelableArrayListExtra("albumsList");

        //Connect Buttons & Fields to XML
        searchButton = (Button) findViewById(R.id.searchButton);
        personSearch = (EditText) findViewById(R.id.personSearch);
        locationSearch = (EditText) findViewById(R.id.locationSearch);
        searchOrAnd = (RadioGroup) findViewById(R.id.searchOrAnd);

        searchButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){ searchButton();}
        });
    }

    //For search button
    public void searchButton(){
        try {
            searchedPhotos = new ArrayList<>();
            boolean boolFilled = false;
            boolean isOr = true;
            String condition = "";
            String person = personSearch.getText().toString().toLowerCase().trim();
            String location = locationSearch.getText().toString().toLowerCase().trim();

            // get selected radio button from radioGroup
            int selectedId = searchOrAnd.getCheckedRadioButtonId();
            // find the radiobutton by returned id
            orAnd = (RadioButton)findViewById(selectedId);
            // get string from selected radio button
            if(orAnd != null && (location != null || !location.equals(""))){
                condition = orAnd.getText().toString().toLowerCase();
                boolFilled = true;
                if(condition.equals("or"))
                    isOr = true;
                else if (condition.equals("and"))
                    isOr = false;
            }

            if(!person.equals("") && !location.equals("") && orAnd == null){
                Toast.makeText(this, "Please Pick 'OR' or 'AND'", Toast.LENGTH_SHORT).show();
            }

            //if it is just location
            if(person.equals("") || person == null){
                for(int i = 0; i < listOfAlbums.size(); i++){
                    Album a = listOfAlbums.get(i);
                    for(int j = 0; j < a.listOfPhotos.size(); j++){
                        Photo ptemp = listOfAlbums.get(i).listOfPhotos.get(j);
                        for(int t = 0; t < ptemp.getListOfTags().size(); t++){
                            Tag temp = listOfAlbums.get(i).listOfPhotos.get(j).getListOfTags().get(t);
                            //Find Tag 2
                            if(temp.getTagName().toLowerCase().trim().equals("location") && temp.getTagValue().toLowerCase().trim().contains(location) && !location.equals("")){
                                searchedPhotos.add(ptemp);
                                break;
                            }
                        }
                    }
                }
            }
            else {
                //Loop through all Albums
                for(int i = 0; i < listOfAlbums.size(); i++){
                    Album a = listOfAlbums.get(i);
                    //Loop through all Photos in album
                    for(int j = 0; j < a.listOfPhotos.size(); j++){
                        boolean personFound = false;
                        boolean locationFound = false;
                        Photo ptemp = listOfAlbums.get(i).listOfPhotos.get(j);
                        //Loop through all Tags in photo
                        for(int t = 0; t < ptemp.getListOfTags().size(); t++){
                            Tag temp = listOfAlbums.get(i).listOfPhotos.get(j).getListOfTags().get(t);

                            //Find Tag 1
                            if(!personFound && temp.getTagName().toLowerCase().trim().equals("person") && temp.getTagValue().toLowerCase().trim().contains(person)){
                                personFound = true;
                                if(!boolFilled || isOr || (!isOr && location.equals(""))){
                                    searchedPhotos.add(ptemp);
                                    break;
                                }
                            }
                            //Find Tag 2
                            if(temp.getTagName().toLowerCase().trim().equals("location") && temp.getTagValue().toLowerCase().trim().contains(location) && !location.equals("")){
                                locationFound = true;
                                if(!(isOr == false && personFound == false)){
                                    searchedPhotos.add(ptemp);
                                    break;
                                }
                            }
                        }
                        if(isOr == false && locationFound == true && personFound == true)
                            searchedPhotos.add(ptemp);
                    }
                }
            }

            if(searchedPhotos.size() == 0){
                Toast.makeText(this, "No Photos Match These Conditions", Toast.LENGTH_SHORT).show();
            }

            //instantiate
            searchedPhotosGrid = (GridView)findViewById(R.id.searchedPhotos);
            adapter = new GridViewAdapter(searchedPhotos,this);
            searchedPhotosGrid.setAdapter(adapter);

        } catch (Exception e) {
            Toast.makeText(this, "Please Fill All Appropriate Fields", Toast.LENGTH_SHORT).show();
        }
    }

}
